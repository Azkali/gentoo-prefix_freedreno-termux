# Gentoo RAP prefix for Termux

## Setup

In `/data/gentoo64/etc/portage/repos.conf/` create the file `freedreno_termux_prefix.conf` with the following content :
```
[freedreno_termux_prefix]
priority = 50
auto-sync = yes
clone-depth = 0
sync-type = git
location = /data/gentoo64/var/db/repos/freedreno_termux_prefix
sync-uri = https://gitlab.azka.li/azkali/gentoo-prefix_freedreno-termux.git
```

See the Gentoo wiki page at https://wiki.gentoo.org/wiki//etc/portage/repos.conf for usage.
If auto-sync is set to yes, the repository should be automatically updated when you update your system.

## Usage

The overlay provides profiles that enhances the gentoo portage profile by providing freedreno specific configurations and packages.
```
# eselect profile list
Available profile symlink targets:
  [1]   default/linux/arm64/17.0/prefix/kernel-3.2+ (exp)
  [2]   freedreno_termux_prefix:freedreno-termux (exp)
```
